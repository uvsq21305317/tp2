package exos;

/**
 * 
	This class is used to create and simulate an user account. 
	Its methods are used to control the actions performed on money.
 *
 *
	This is just a test
 *  
 *  @version
		1.0 XX/YY/ZZ
 *	@author
		Kael Ferret
 */

public class Account {
	
	/*This is a class implementation comment*/
	
	/**
	 		"flouz" variable countains how much money there is on given account */
	int flouz;

	/**
	 According to the tests, init() constructs an instance of Account.
	 @param value	Quantity of money which you create the Account with
	 */
		public void init(int value)
		{
			if(value > 0)
			{
				this.flouz = value;
			}
			else
			{
				this.flouz = 0;
				System.out.print("Can't use negative values. Account set to 0.");
			}
		}
		
		
		/**
		 * Show how much money there is on the account
		 * @return string	return following string : "argent : " int "\n"
		 */
public String show()
		{
			return "Argent sur le compte : " + this.flouz + "\n";
		// Not sure how to talk about implementation
			}
		
		
	/**
	 Adds money to current account
	 @param value	Add "value" amount of money to current account
	 */
public void add(int value)
		{
			if(value > 0)
			{
				this.flouz += value;
			}
			else
			{
				System.out.print("Can't use negative values. Nothing changed.");
			}
		//LOl i dunno implementation2
			}
		
		
	/**
	 Remove money to the account
	 @param value	remove "value" amount of money to current Account
	 */
		public void remove(int value)
		{
			if(value < this.flouz)
			{
				this.flouz -= value;
			}
			else
			{
				System.out.print("Operation refused. You do not have enough money.");
			}
		}
		/**
		 Transfer the value passed as parameter from the Account A to current instance of Account
		 @param A 		the account which gives money
		 @param value	how much money the account gives
		 */
		
public void move(Account A, int value)
		{
			if(value > 0 && A.flouz > value)
			{
				A.remove(value);
				this.add(value);
			}
			else
			{
				this.flouz = 0;
				System.out.print("Operation DENIED.");
			}
			
		//lol implmentation
		}
}
