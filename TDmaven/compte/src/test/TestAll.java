package exos;

import static org.junit.Assert.*;
import org.junit.*;

/* test list
 * initAccount(unsigned int), est ce que je peux créer un compte avec une solde ? 
 * showAccount(), est ce que je peux voir la quantité d'argent ?
 * addMoney(unsigned int), est ce que je peux ajouter de l'argent ?
 * rmMoney(unsigned int), est ce que je peux retirer de l'argent ? Uniquement si le solde final > 0
 * moveMoney(account), est ce que je peux bouger de l'argent vers le compte en paramètre ?
 */

public class TestAll
{
@Test
public void InitTest()
{
	Account A = new Account();
	A.init(500);
	assertEquals(500, A.flouz);
}
@Test
public  void testShow()
{
	Account A = new Account();
	A.init(500);
	assertEquals("Argent sur le compte : 500\n", A.show());
	
}

@Test
public void testAdd()
{
	Account A = new Account();
	A.init(500);
	A.add(200);
	assertEquals(700, A.flouz);
}
@Test
public void testRemove()
{ 
	Account A = new Account();
	A.init(500);
	A.remove(400);
	assertEquals(500-400, A.flouz);
	
	//test pour l'exception
	try
	{
		A.remove(-2000);
	}
	catch (IllegalStateException ise)
	{
		assertTrue(true);
	}
}
@Test
public void testMove()
{
	Account A = new Account();
	A.init(500);
	Account B = new Account();
	B.init(500);
	
	A.move(B, 200);
	assertEquals(500+200, A.flouz);
	assertEquals(500-200, B.flouz);
	
	//second test pour l'exception
	try
	{
		A.move(B, -2000);
	}
	catch (IllegalStateException ise)
	{
		assertTrue(true);
	}
	
}

}